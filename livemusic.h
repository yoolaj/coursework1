#ifndef _LIVEMUSIC_H_
#define _LIVEMUSIC_H_
#include "events.h"
#include <string>

class Livemusic:public Events{
 private:
  string liveId;
  string liveName;
  int maximum_number;
 public:
  Livemusic();
  virtual ~Livemusic();
  string addbooking();
  string cancelbooking();
  string checkmaxattendance();

};

#endif
