/*
~Author: M00751032
~Date created: 22/12/2021
~Last modified: ###
*/
#include <iostream>
#include<fstream>
#include "events.h"
#include "livemusic.h"
#include "standupcom.h"
#include "film.h"


using namespace std;
int MainMenu();

int AddBooking(){
  string uInput;
  int numUInput;
  Events EventsObj;
  string select;
  char ch;

   cout << "\nThese are the events available\n" << endl;
  
  const char *fileName="Events.txt";
	
//declare object
  ifstream file;
	
  file.open(fileName,ios::in); //open file
  if(!file)
    {
      cout<<"Error in opening file!!!"<<endl;
      return -1; //return from main
    }
	
   while (!file.eof())
     {
        file >> noskipws >> ch;	//reading from file
	cout << ch;	//printing
     }
   
    file.close(); //close the file

    cout << "\nPlease select your choice of event from the list above:" << endl;
 cin >> select;

 //code for livemusic booking
      if (select == "1"){
	cout << "You chose live music.\nAre you sure you want to book this event (Y/N)\n";
	cin >> select;

	if (select == "Y" || select == "y"){

	  cout << "Please enter the number of bookings to be reserved: ";
	  cin >> uInput;
	  
	  int seats = 300;
	  numUInput = stoi(uInput);
	  EventsObj.setEventSeatsAvailable(numUInput);
	  EventsObj.getEventSeatsAvailable();
	  if((seats-numUInput) <0){
	    cout << "Limit exceeded\n Booking failed";
	  }else{
	     cout << "Seats reserved\n";

	      ofstream outfile;
	      outfile.open("livemusic.txt");
	      string str = "L01";
	      outfile <<str<< ", " << uInput << endl;
	      outfile.close();
	  
	      cout << "Booking added";
	  }
	}
	else if (select == "N" || select == "n"){
	  AddBooking();
	}
	else{
	  cout << "Invalid selection, please try again\n";
	  AddBooking();
	}
	  
     }

      // code for standup comedy
      else if (select == "2"){
	cout << "You have chosen stand-up comedy\nChoose the seat you want\n Select from 1 to 200\n";

	  cin >> uInput;

	  if (stoi(uInput) >= 1  && stoi(uInput) <= 200){
	  
	  int seats = 200;
	  numUInput = stoi(uInput);
	  EventsObj.setEventSeatsAvailable(numUInput);
	  EventsObj.getEventSeatsAvailable();
	  if((seats-numUInput) <0){
	    cout << "Limit exceeded\n Booking failed";
	  }else{
	     cout << "Seat selected\n";

	      ofstream outfile;
	      outfile.open("standupcom.txt");
	      string id = "SU01";
	      outfile <<id<< ", seat" << uInput << endl;
	      outfile.close();
	  
	      cout << "Booking added";
	  }
	}
	else{
	  cout << "Invalid selection, please try again\n";
	  AddBooking();
	}
       
     }

      //code for film selection
      else if (select == "3"){
	cout << "Select type of view(Choose either 2D or 3D)\n";
	cin >> select;

	if (select == "2D" || select == "2d"){

	  cout << "Please enter the number of seats to be reserved: ";
	  cin >> uInput;
	  
	  int seats = 200;
	  numUInput = stoi(uInput);
	  EventsObj.setEventSeatsAvailable(numUInput);
	  EventsObj.getEventSeatsAvailable();
	  if((seats-numUInput) <0){
	    cout << "Limit exceeded\n Booking failed";
	  }else{
	     cout << "Seats reserved\n";

	      ofstream outfile;
	      outfile.open("film.txt");
	      string id = "F01";
	      string view = "2D";
	      outfile << id << ", " << view << ", " << uInput << endl;
	      outfile.close();
	  
	      cout << "Booking added";
	  }
	}
	else if (select == "3D" || select == "3d"){
	  cout << "Please enter the number of seats to be reserved: ";
	  cin >> uInput;

	  int seats = 200;	  
	  numUInput = stoi(uInput);
	  EventsObj.setEventSeatsAvailable(numUInput);
	  EventsObj.getEventSeatsAvailable();
	  if((seats-numUInput) <0){
	    cout << "Limit exceeded\n Booking failed";
	  }else{
	     cout << "Seats reserved\n";

	      ofstream outfile;
	      outfile.open("film.txt");
	      string id = "F01";
	      string view = "3D";
	      outfile << id << ", " << view << ", " << uInput << endl;
	      outfile.close();
	  
	      cout << "Booking added";
	  }
	}
	else{
	  cout << "Invalid selection, please try again\n";
	  AddBooking();
	}
      }
       

  return 0;
}


int CancelBooking(){
  string select;
  char ch;

  cout << "Please choose which event booking do you want to cancel.\n1. Livemusic\n2. Standup Comedy\n3. Film\n";
  cin >> select;

  if(select == "1"){
      const char *fileName="livemusic.txt";
      string choose;
	
      ifstream live;
	
      live.open(fileName,ios::in); //open file
      if(!live)
	{
	  cout<<"Error in opening file!!!"<<endl;
	  return -1; //return from main
	}

      cout << "These are the bookings available to be canceled\n";
      while(!live.eof())
	{
	  live >> noskipws >> ch;	//reading from file
	  cout << ch;	//printing
	}
      
      live.close(); //close the file
      
      cout << "\nAre you sure you want to cancel this booking? \nSelect y or n where y is Yes and n is No\n";
      cin >> choose;

      if(choose == "y"){
	ofstream file("livemusic.txt");
	file<<"";

	cout << "Booking canceled succefully";
      }
      else if(choose == "n"){
	CancelBooking();
      }
      else{
	cout << "\nInvalid choice, please try again with either y or n";
	CancelBooking();
      }
  }



  if(select == "2"){
     const char *filename="standupcom.txt";
     string choose;
	
     ifstream file;
	
     file.open(filename,ios::in); //open file
     
     if(!file)
       {
	 cout<<"Error in opening file!!!"<<endl;
	 return -1; //return from main
       }

     cout << "These are the bookings available to be canceled\n";
     while (!file.eof())
       {
	 file >> noskipws >> ch;	//reading from file
	 cout << ch;	//printing
       }
     file.close(); //close the file

      cout << "\nAre you sure you want to cancel this booking? \nSelect y or n where y is Yes and n is No\n";
      cin >> choose;

      if(choose == "y"){
	ofstream file("standupcom.txt");
	file<<"";

	cout << "Booking canceled succefully";
      }
      else if(choose == "n"){
	CancelBooking();
      }
      else{
	cout << "\nInvalid choice, please try again with either y or n";
	CancelBooking();
      }
    
  }


  if (select == "3"){
     const char *film="film.txt";
     string choose;
	
  ifstream Film;
	
  Film.open(film,ios::in); //open file
  if(!Film)
    {
      cout<<"Error in opening file!!!"<<endl;
      return -1; //return from main
    }

  cout << "These are the bookings available to be canceled\n";
   while (!Film.eof())
     {
        Film >> noskipws >> ch;	//reading from file
	cout << ch;	//printing
     }
    Film.close(); //close the file

      cout << "\nAre you sure you want to cancel this booking? \nSelect y or n where y is Yes and n is No\n";
      cin >> choose;

      if(choose == "y"){
	ofstream file("film.txt");
	file<<"";

	cout << "Booking canceled succefully";
      }
      else if(choose == "n"){
	CancelBooking();
      }
      else{
	cout << "\nInvalid choice, please try again with either y or n";
	CancelBooking();
      }    
  }

  return 0;
}

int ListEvents(){
  string select;
  char ch;

   cout << "\nThese are the events below\n" << endl;
  
  const char *fileName="Events.txt";
	
//declare object
  ifstream file;
	
//open file
  file.open(fileName,ios::in);
  if(!file)
    {
      cout<<"Error in opening file!!!"<<endl;
      return -1; //return from main
    }

   while (!file.eof())
     {
        file >> noskipws >> ch;	//reading from file
	cout << ch;	//printing
     }
    file.close(); //close the file
	
     cout << "\n1. Back\n2. Exit\n\nPlease select your choice:" << endl;
 
     cin >> select;

     if (select == "1"){
       MainMenu();
     }else if (select == "2"){
       system("clear");
     }else{
       cout <<  "Invalid selection. Please try again!";
       ListEvents();
     }
  return 0;
}

int ListDetails(){
  string choice;
  char ch;
  
  cout << "Below are the events details";
   
  const char *fileName="details.txt";
	
//declare object
  ifstream file;
	
//open file
  file.open(fileName,ios::in);
  if(!file)
    {
      cout<<"Error in opening file!!!"<<endl;
      return -1; //return from main
    }

   while (!file.eof())
     {
        file >> noskipws >> ch;	//reading from file
	cout << ch;	//printing
     }
    file.close(); //close the file
	
     cout << "\n1. Back\n2. Exit\n\nPlease select your choice:" << endl;
 
     cin >> choice;

     if (choice == "1"){
       MainMenu();
     }else if (choice == "2"){
       system("clear");
     }else{
       cout <<  "Invalid selection. Please try again!";
       ListDetails();
     }

  return 0;
}


int MainMenu(){

  string userInput;
  cout << "Welcome!\nPlease pick a choice from the choices below:\n1.Add booking\n2.Cancel booking\n3.List all events\n4.List details\n5.Exit\n\nPlease enter your choice:\n" << endl;
  cin >> userInput;

  if (userInput == "1"){
    AddBooking();
  }else if (userInput == "2"){
    CancelBooking();
  }else if (userInput == "3"){
    ListEvents();
  }else if (userInput == "4"){
    ListDetails();
  }else if (userInput == "5"){
    system("clear");
  }else{
    cout <<  "Invalid selection. Please try again!";
    MainMenu();
  }
  
  return 0;

}




int main(){
  MainMenu();
  

  return 0;
}
