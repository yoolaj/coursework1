#ifndef _EVENTS_H_
#define _EVENTS_H_

#include <string>

using namespace std;

class Events {
private:
  std::string name;
  std::string type;
  int seats_available;
 public:
  Events();
  virtual ~Events();
  std::string addbooking();
  std::string cancelbooking();
  std::string listevents();
  std::string listdetails();
  std::string getbooking();
  int setEventSeatsAvailable(int seats_available);
  
  int getEventSeatsAvailable();

};


#endif
