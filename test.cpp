#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "events.h"
#include "livemusic.h"
#include "standupcom.h"
#include "film.h"

TEST_CASE("Events class Test", "[Events()]")
{
  Events events;
  events.addbooking("Film");
  events.cancelbooking();
  events.listevents("");
  events.listdetails();
  events.getbooking();
  events.setEventSeatsAvailable("200");
 

 
  REQUIRE(events.getEventSeatsAvailable() == "200");
 
}
