#ifndef _FILM_H_
#define _FILM_H_
#include "events.h"
#include <string>

class Film : public Events {
 private:
  string filmId;
  string filmName;
  int maximum_number;
 public:
  Film();
  virtual ~Film();
  string addbooking();
  string cancelbooking();
  string getSeat();
  string checkmaxattendance();
};

#endif
