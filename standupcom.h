#ifndef _STANDUPCOM_H_
#define _STANDUPCOM_H_
#include "events.h"
#include <string>

class Standup_Comedy : public Events {
 private:
  string scId;
  string scName;
  int seatNumber;
  int maximum_number;
 public:
  Standup_Comedy();
  virtual ~Standup_Comedy();
  string addbooking();
  string cancelbooking();
  string setSeat();
  string getSeat();
  string checkSeat();

};

#endif
