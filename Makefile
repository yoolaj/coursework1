CXX = g++
CXXFLAGS = -g -Wall -Wextra

.PHONY: all
all: main

main: main.cpp events.o livemusic.o standupcom.o film.o
	$(CXX) $(CXXFLAGS) -o $@ $^

film.o: film.cpp events.o
	$(CXX) $(CXXFLAGS) -c $^

standupcom.o: standupcom.cpp events.o
	$(CXX) $(CXXFLAGS) -c $^

livemusic.o: livemusic.cpp events.o
	$(CXX) $(CXXFLAGS) -c $^

events.o: events.cpp
	$(CXX) $(CXXFLAGS) -c $<

.PHONY: clean
clean:
	rm*~ *.o main.exe
